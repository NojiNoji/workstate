<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class MomentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Moments')->insert([
            [
            'user_id' => 1,
            'state' => 0,
            'state_cat' => 3,
            ],
            [
            'user_id' => 1,
            'state' => 1,
            'state_cat' => 2,
            ],

            [
            'user_id' => 2,
            'state' => 2,
            'state_cat' => 3,
            ],

        ]);
    }
}
