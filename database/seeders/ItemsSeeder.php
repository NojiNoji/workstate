<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ItemsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Items')->insert([
            ['user_id' => 1,
            'item' => 'harumaki',
            'category' => 'default',
            'status' => 'high'],
            ['user_id' => 1,
            'item' => 'poteto',
            'category' => 'default',
            'status' => 'low'],
            ['user_id' => 1,
            'item' => 'tamago',
            'category' => 'default',
            'status' => 'high'],
            ['user_id' => 2,
            'item' => 'onaka',
            'category' => 'default',
            'status' => 'high'],
            ['user_id' => 2,
            'item' => 'saba',
            'category' => 'sakana',
            'status' => 'high'],
            ['user_id' => 3,
            'item' => 'oishii',
            'category' => 'gohan',
            'status' => 'high']
        ]);
    }
}
