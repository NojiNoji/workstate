<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    public function up()
    {
        Schema::create('Items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('item');
            $table->string('addition', 255)->nullable();
            $table->string('category');
            $table->string('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('Items');
    }
}
