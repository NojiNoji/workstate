<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Moments extends Model
{
    use HasFactory;

    protected $fillable = [
        'state_cat',
        'user_id',
        'timestamps',
        'state'
    ];

    protected $guarded = [
        'id',
    ];

    public function moments() {
        return $this->belongsTo('App\Models\Moments');
    }

}
