<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items extends Model
{
    use HasFactory;

    protected $fillable = [
        'item',
        'category',
        'status',
        'user_id',
    ];

    protected $guarded = [
        'id',
    ];

    public function user() {
        return $this->belongsTo('App\Models\User');
    }
}
