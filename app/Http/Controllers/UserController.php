<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Items;
use Inertia\Inertia;

class UserController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $user_id = Auth::user()->id;
        $items = Items::where('user_id', $user_id)->get();

        return Inertia::render('User/Index',[
            'items' => $items,
            'user' => $user,
        ]);
    }

    public function store(Request $request)
    {
        Validator::make($request->all(), [
            'user_id' => ['required', 'max:50'],
            'item' => ['required', 'max:50'],
            'category' => ['required', 'max:50'],
            'status' => ['required', 'max:50'],
        ])->validate();
        Items::create([
            'user_id' => $request->get('user_id'),
            'item' => $request->get('item'),
            'category' => $request->get('category'),
            'status' => $request->get('status'),
        ]);

        return redirect('users');
    }

    public function state_update(Request $request)
    {
        $user = Auth::user();
        $user->state = $request->get('current');
        $user->state_second =  $request->get('second');
        $user->state_third =  $request->get('third');
        $user->save();

        return redirect('users');
    }

    public function update(Request $request, $id)
    {
        $item = Items::find($id);
        $item->item = $request->get('item');
        $item->category =  $request->get('category');
        $item->status =  $request->get('status');
        $item->save();

        return redirect('users');
    }

    public function destroy($id)
    {
        $item = Items::findOrFail($id);
        $item->delete();

        return redirect('users');
    }
}

