<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Items;
use App\Models\Moments;
use Inertia\Inertia;



class DashboardController extends Controller
{
    public function index()
    {
        $items = Items::all();
        $manager = DB::table('users')
            ->where('group', 'マネージャー')
            ->select('users.id', 'users.name', 'users.state', 'users.state_second', 'users.state_third', 'profile_photo_path')
            ->get();
        $designer = DB::table('users')
            ->where('group','デザイナー')
            ->select('users.id', 'users.name', 'users.state', 'users.state_second', 'users.state_third', 'profile_photo_path')
            ->get();
        $engineer = DB::table('users')
            ->where('group','エンジニア')
            ->select('users.id', 'users.name', 'users.state', 'users.state_second', 'users.state_third', 'profile_photo_path')
            ->get();
        return Inertia::render('Dashboard',[
            'items' => $items, 
            'manager' => $manager,
            'designer' => $designer,
            'engineer' => $engineer,
            'relayToUser' => User::with('items')->get(),
        ]);
    }
    


}
