<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\Items;
use App\Models\Moments;
use Inertia\Inertia;


class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $maxMoments = Array();
        $status = Array();

        for ($i = 0; $i < 3; $i++) {
            $catNum = $i + 1;
            $momentMax = Moments::where('state_cat', (String) $catNum)->where('user_id', $id)->max('updated_at');
            if($momentMax) {
                $momentMaxRecord = Moments::where('updated_at', $momentMax)->first();
            } else {
                $momentMaxRecord = null;
            }
            array_push($maxMoments, $momentMaxRecord);
        }

        foreach ($maxMoments as $index => $value) {
            array_push($status, $value);
        }

        return $status;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $status = Moment::where('user_id', $id)->all();
        
        foreach ($status as $state) {
            $state->state = $request->get('status');
            $state->state_cat = $request->get('status_cat');
            $state->save();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
