<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    // 使用後にテストデータを削除
    // use RefreshDatabase;

    /**
     * Private function  ログイン用のダミーユーザーを作成し、ログイン 
     */
    private function dummyLogin()
    {
        $user = User::factory()->make();
        
        return $this->actingAs($user)
            ->withSession(['user_id' => $user->id])
            ->get('/');
    }
    /****/


    // ログイン画面を表示する
    public function testViewLogin()
    {
        $response = $this->get('login');

        $response->assertStatus(200);
        $this->assertGuest();
    }

    // ログインする
    public function testLogin()
    {
        // ゲストか確認
        $this->assertGuest();

        // Login
        $response = $this->dummyLogin();

        // ログインできているか確認
        $this->assertAuthenticated();
    }

    // ダッシュボードにアクセスする
    public function testViewDashboard()
    {
        // Login view
        $response = $this->get('login');
        // Login
        $this->dummyLogin();

        $response = $this->get('/');
        $response->assertStatus(200);

    }
}
