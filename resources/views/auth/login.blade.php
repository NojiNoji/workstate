<x-guest-layout>
    <x-jet-authentication-card>
        <h1>
            <div class="flex justify-center items-center relative">
                <div class="bg-white">
                    <svg xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="#d6d867"
                        class="w-13 mx-auto absolute top-0 left-0 right-0"
                    >
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM8 17.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5zM9.5 8c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5S9.5 9.38 9.5 8zm6.5 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/>
                    </svg>
                </div>
                <div class="title-logo text-lg pt-12">
                    <div class="inline-block">
                        <span class="relative inline-block text-white font-bold"
                            style="color: rgb(216, 103, 103);"
                        >
                            W
                        </span>ork
                    </div>
                    <div class="inline-block">
                        <span class="relative inline-block text-white font-bold"
                            style="color: rgb(118, 144, 228);"
                        >
                            S
                        </span>tate
                    </div>
                    <div class="inline-block">
                        <span class="relative inline-block text-white font-bold"
                            style="color: rgb(116, 202, 116);">
                            S
                        </span>hare
                    </div>
                </div>
            </div>
        </h1>
        <x-slot name="logo">
        
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-jet-label value="Email" />
                <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <div class="mt-3">
                <x-jet-label value="パスワード" />
                <x-jet-input class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>



            <div class="flex items-center justify-center mt-5">
                <x-jet-button>
                    ログイン
                </x-jet-button>
                {{-- 21e35c6a --}}
            </div>
            <div class="block mt-3">
                <label class="flex items-center justify-center">
                    <input type="checkbox" class="form-checkbox" name="remember">
                    <span class="ml-2 text-xs text-gray-600">ログイン情報を記憶する</span>
                </label>
            </div>
        </form>
        <p class="absolute" style="margin-top: 50px;font-size: 6px;">
            テストアカウント<br>
            ID : test@test.com<br>
            pass : testtest<br><br>
            ※このアプリケーションはポートフォリオです。<br> ※人物名は全て架空です。
        </p>
    </x-jet-authentication-card>
</x-guest-layout>