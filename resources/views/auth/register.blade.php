<x-guest-layout>
    <x-jet-authentication-card>
        <h1>
            <div class="flex justify-center items-center relative">
                <div class="bg-white">
                    <svg xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        fill="#dcdd82"
                        class="w-13 mx-auto absolute top-0 left-0 right-0"
                    >
                        <path d="M0 0h24v24H0z" fill="none"/>
                        <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zM8 17.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5zM9.5 8c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5S9.5 9.38 9.5 8zm6.5 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/>
                    </svg>
                </div>
                <div class="title-logo text-lg pt-12">
                    <div class="inline-block">
                        <span class="relative inline-block text-white font-bold"
                            style="color: rgb(216, 103, 103);"
                        >
                            W
                        </span>ork
                    </div>
                    <div class="inline-block">
                        <span class="relative inline-block text-white font-bold"
                            style="color: rgb(118, 144, 228);"
                        >
                            S
                        </span>tate
                    </div>
                    <div class="inline-block">
                        <span class="relative inline-block text-white font-bold"
                            style="color: rgb(116, 202, 116);">
                            S
                        </span>hare
                    </div>
                </div>
            </div>
        </h1>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        <x-jet-validation-errors class="mb-4" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div>
                <x-jet-label value="名前" />
                <x-jet-input class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name" />
            </div>

            <div class="mt-4">
                <x-jet-label value="所属" />
                <select name="group" class="block mt-1 w-7/12 rounded-sm h-7 border-gray-300 px-3 py-0.5 shadow-sm form-select text-xs">
                    <option>マネージャー</option>
                    <option>デザイナー</option>
                    <option>エンジニア</option>
                </select>
            </div>

            <div class="mt-4">
                <x-jet-label value="Email" />
                <x-jet-input class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label value="パスワード" />
                <x-jet-input class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label value="パスワード(確認)" />
                <x-jet-input class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-xs text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('ログイン') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('登録する') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
