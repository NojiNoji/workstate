import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        myStore: {
            stateBand: {
                stickyShow: 0,
            }
        }
    },
    mutations: {
        commitStickyShow(state, payload) {
            state.myStore.stateBand.stickyShow = payload;
        }
    },  
    getters: {
        getStickyShow(state) {
            return state.myStore.stateBand.stickyShow;
        }
    }
})