require('./bootstrap');

import Vue from 'vue';
import Vuex from 'vuex';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

import { InertiaApp } from '@inertiajs/inertia-vue';
import { InertiaForm } from 'laravel-jetstream';
import PortalVue from 'portal-vue';

import store from './store';

Vue.use(InertiaApp);
Vue.use(store);
Vue.use(InertiaForm);
Vue.use(PortalVue);
Vue.use(Vuetify);

const app = document.getElementById('app');

new Vue({
    moment: new moment(),
    vuetify: new Vuetify(),
    store,
    render: (h) =>
        h(InertiaApp, {
            props: {
                initialPage: JSON.parse(app.dataset.page),
                resolveComponent: (name) => require(`./Pages/${name}`).default,
            },
        }),
}).$mount(app);
